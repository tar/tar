You might like

`deb2itp`, `git2deb`, and `abp` from

https://github.com/alexmyczko/autoexec.bat

Short summary on Debian packaging

https://github.com/alexmyczko/autoexec.bat/blob/master/Documents/debian-packaging.md

`mdp` presentation about Debian and Linux on Apple M1 and M2

https://github.com/alexmyczko/autoexec.bat/blob/master/Documents/debian.md

https://github.com/alexmyczko/autoexec.bat/blob/master/Documents/bananas.md
